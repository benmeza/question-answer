import React, { Component } from 'react';
import './App.css';

// child functional components
import QuestionLabel from './components/question';
import BackButton from './components/back-button';
import NextButton from './components/next-button';
import Responses from './components/responses';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      pageIndex: 0,
      responses: [
        {
          id: 1,
          question: 'Tell us a little bit about yourself and your experiences as a software engineer?',
          answer: '',
          img: 'https://github.com/pabo/react-app-interview/raw/master/images/icon-accurate-retina.png',
        },{
          id: 2,
          question: 'Describe your dream job and tell me what your daily activities would be?',
          answer: '',
          img: 'https://github.com/pabo/react-app-interview/raw/master/images/icon-max-refund-retina.png',
        },{
          id: 3,
          question: 'When can you start?',
          answer: '',
          img: 'https://github.com/pabo/react-app-interview/raw/master/images/icon-audit-support-retina.png',
        }
      ],
    };
    this.textInput = React.createRef();
  }

  nextQuestion = () => {
    const { pageIndex, responses } = this.state;
    if (pageIndex !== responses.length) {
      this.setState((prevState) => {
        return {
          pageIndex: prevState.pageIndex + 1
        };
      });
    }
    this.textInput.current.focus();
  };

  backQuestion = () => {
    if (this.state.pageIndex === 0) return;
    this.setState((prevState) => {
      return {
        pageIndex: prevState.pageIndex - 1
      };
    });
    if (this.textInput.current) { this.textInput.current.focus(); }
  };

  updateResponse = (event) => {
    const value = event.target.value;
    const { pageIndex } = this.state;
    this.setState((prevState) => {
      const oldResp = prevState.responses;
      oldResp[pageIndex].answer = value;
      return {
        responses: oldResp
      };
    });
  };

  render() {
    const { pageIndex, responses } = this.state;
    const currentResponse = responses.find((item) => item.id === (pageIndex + 1));
    const firstQuestion = pageIndex > 0;
    const isResponsePage = pageIndex < responses.length;
    return (
      <div className="Main">
        <div className="App">
          {isResponsePage ? (
            <img src={currentResponse.img} alt="top" className="App-logo" />
          ) : (
            <Responses responses={responses} />
          )}
          <div className="Sub-Main">
          {isResponsePage && 
            <div>
              <QuestionLabel question={currentResponse.question} />
              <input ref={this.textInput} autoFocus className="Answer-Text" type="text" value={currentResponse.answer} onChange={this.updateResponse} />
            </div>
          }
            <div className="Div-Button">
              <div style={{ alignSelf: 'flex-end' }}>{firstQuestion && <BackButton onBack={this.backQuestion} />}</div>
              <div style={{ alignSelf: 'flex-start' }}>{isResponsePage && <NextButton onNext={this.nextQuestion} />}</div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default App;
