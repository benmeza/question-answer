import React from 'react';
import PropTypes from 'prop-types';

const Responses = (props) => {
  const { responses } = props;

  return (
    <div>
      <h1 style={{ textAlign: 'center', margin: '32px' }}>Summary</h1>
      {responses.map(value => (
        <div key={value.id}>
          <h4>{`#${value.id}: ${value.question}`}</h4>
          {value.answer === '' ? (
            <h5 style={{ color: 'red' }}>no response given</h5>
          ) : (
            <h5>{`A: ${value.answer}`}</h5>
          )}
          <hr />
        </div>
      ))}
    </div>
  );
};

Responses.propTypes = {
  responses: PropTypes.arrayOf(PropTypes.shape({
    id: PropTypes.number.isRequired,
    question: PropTypes.string.isRequired,
    answer: PropTypes.string.isRequired,
  })).isRequired,
};

export default Responses;
