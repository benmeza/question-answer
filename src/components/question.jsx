import React from 'react';
import PropTypes from 'prop-types';

const QuestionLabel = (props) => {
  const { question } = props;
  return (
    <div style={{ margin: '32px 0' }}>
      <p>
        {question}
      </p>
    </div>
  );
};

QuestionLabel.propTypes = {
  question: PropTypes.string.isRequired,
};

export default QuestionLabel;
