import React from 'react';
import PropTypes from 'prop-types';

const BackButton = (props) => {
  const { onBack } = props;
  return (
    <button type="button" onClick={onBack} style={{ backgroundColor: '#F7C7C5', border: '3px solid red' }}>
      Back
    </button>
  );
};

BackButton.propTypes = {
  onBack: PropTypes.func.isRequired,
};

export default BackButton;
