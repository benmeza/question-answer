import React from 'react';
import PropTypes from 'prop-types';

const NextButton = (props) => {
  const { onNext } = props;
  return (
    <button type="button" onClick={onNext} style={{ backgroundColor: '#CFE4CE', border: '3px solid green' }}>
      Next
    </button>
  );
};

NextButton.propTypes = {
  onNext: PropTypes.func.isRequired,
};

export default NextButton;
